﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class gameSession
{
    //has the game started?
    public bool gameStarted,isMyTurn;    

    public gameSession()
    {
        isMyTurn = false;
        gameStarted = false;
    }    

    public void startGame()
    {
        gameStarted = true;
    }
}

public class Player
{
    public string PlayerName;
    public bool isHisTurn, inGame;
    
      
}

public class gameManager : MonoBehaviour
{
    public gameSession session;
    public FirebaseScript dbScript;
    public Player currentPlayer, otherPlayer;
    GameObject timerText, theTimer;
    int playercounter = 1;
    public string currentPlayerKey,enemyPlayerKey;
    bool starts = false;
    bool timerrunning = false;
    //string nickname;
   

    IEnumerator clearDB()
    {
        
        yield return dbScript.clearFirebase();
        Application.Quit();
    }
    
    IEnumerator waitForOtherPlayer()
    {
        yield return dbScript.getNumberOfRecords();
        playercounter = dbScript.numberOfRecords;

        while (playercounter<2)
        {
            //first player to join starts
            starts = true;

            Debug.Log("Waiting for other player" + Time.time);
            yield return dbScript.getNumberOfRecords();
            playercounter = dbScript.numberOfRecords;
           
            
            //lobby
        }
        //if another player joins the game can begin
        Player otherPlayer = new Player();
        //I need to get the key of the OTHER player
        
        yield return dbScript.getOtherPlayerKey(otherPlayer, this);

        //I now have the other player's key.  Let's randomly choose whose turn is next. 
        Debug.Log("other player has joined");

       // session.isMyTurn = true;



        


        yield return null;
    }
    
    IEnumerator addPlayerToFirebase()
    {
        currentPlayer = new Player();
        
        currentPlayer.PlayerName = "P"+playercounter;
        currentPlayer.isHisTurn = true;
        currentPlayer.inGame = false;        
        
        //yield return dbScript.clearFirebase();

        //currentPlayer.PlayerName = nickname;
        
        yield return dbScript.addDataClass(JsonUtility.ToJson(currentPlayer),this);

        yield return waitForOtherPlayer();
    }

    public IEnumerator BeginGame()
    {
        yield return dbScript.initFirebase();
        yield return addPlayerToFirebase();
        
        //timerText.GetComponentInChildren<Text>().text = "00:00";
                
        //session.startGame();
        //StartCoroutine(updateTimer());
        
       
        

        //start the turns. 
        /*if (starts)
        {
            session.isMyTurn = true;
            while (true) {
                if (session.isMyTurn) {
                    //Debug.Log("my turn!");
                    yield return null;
                }
                else
                {
                    //Debug.Log("their turn!");
                    yield return null;
                }
            }
        }
        else
        {
            while (true)
            {
                if (session.isMyTurn) {
                    //Debug.Log("nmy turn!");
                    yield return null;
                }
                else
                {
                    //Debug.Log("ntheir turn!");
                    yield return null;
                }
            }
        }*/
        
        yield return null;
    }

    /*public IEnumerator updateTimer()
    {
        float timerValue = 0f;
     
        Text clockText = theTimer.GetComponentInChildren<Text>();

        timerrunning = true;

       // clockText.text = "00:00";
       while (true) { 
            if (session.isMyTurn)
            {
                timerValue++;

                float minutes = timerValue / 60f;
                float seconds = timerValue % 60f;

                clockText.text = string.Format("{0:00}:{1:00}", minutes, seconds);


                //code that is running every second
                yield return new WaitForSeconds(1f);
            }
            yield return null;
        }
        
    }*/

    void Start()
    {
        //add the firebase script to the main camera
        Camera.main.gameObject.AddComponent<FirebaseScript>();

        dbScript = Camera.main.GetComponent<FirebaseScript>();

        //theTimer = Instantiate(timerText, new Vector3(-18f, 19f), Quaternion.identity);

        //session = new gameSession();



        StartCoroutine(BeginGame());


      
    }

    public IEnumerator waitForTurn()
    {
        // yield return new WaitForSeconds(10f);
        //session.isMyTurn = true;

        yield return null;
    }
    
    // Update is called once per frame
    void Update()
    {
       //nickname = GameObject.Find("NicknameField").GetComponent<InputField>().text;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(clearDB());
        }

        /*if (Input.GetKeyDown(KeyCode.S))
        {
            StartCoroutine(dbScript.uploadScreenshot());
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            StartCoroutine(dbScript.downloadAndSaveImage());
        }*/
    }
}