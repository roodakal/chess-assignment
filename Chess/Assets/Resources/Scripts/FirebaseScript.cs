﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Extensions;
using Firebase.Unity.Editor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System.IO;


public class FirebaseScript : MonoBehaviour
{
    DatabaseReference reference;
    FirebaseStorage storage;
    public int numberOfRecords = 0;
    Dictionary<string, object> myDataDictionary;
    FirebaseAuth auth;
        
    string email, password;
    GameObject addUserButton, loginButton, getDataButton;
    
    public IEnumerator addDataClass(string datatoinsert,gameManager g)
    {
        //create a unique ID
        string newkey = reference.Push().Key;
        Debug.Log(newkey);
        //the key for the current player
        g.currentPlayerKey = newkey;
        //Update the unique key with the data I want to insert
        yield return StartCoroutine(updateDataClass(newkey, datatoinsert));
    }

    IEnumerator updateDataClass(string childlabel, string newdata)
    {
        //find the child of player4 that corresponds to playername and set the value to whatever is inside newdata
        Task updateJsonValueTask =  reference.Child(childlabel).SetRawJsonValueAsync(newdata).ContinueWithOnMainThread(
            updJsonValueTask =>
            {
                if (updJsonValueTask.IsCompleted)
                {
                   // dataupdated = true;
                }

            });


        yield return new WaitUntil(() => updateJsonValueTask.IsCompleted);




    }

   public IEnumerator clearFirebase()
    {
        Task removeAllRecords = reference.RemoveValueAsync().ContinueWithOnMainThread(
            rmAllRecords =>
            {
                if (rmAllRecords.IsCompleted)
                {
                    Debug.Log("Database clear");
                }
            });

        yield return new WaitUntil(() => removeAllRecords.IsCompleted);

    }

    IEnumerator createUser()
    {
        auth = FirebaseAuth.DefaultInstance;

        Task createusertask = auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(
            //created an anonymous inner class inside continueonmainthread which is of type Task
            createUserTask =>
            {
                //if anything goes wrong
                if (createUserTask.IsCanceled)
                {
                    //I pressed escape or cancelled the task
                    Debug.Log("Sorry, user was not created!");
                    return;
                }
                if (createUserTask.IsFaulted)
                {
                    //my internet exploded or firebase exploded or some other error happened here
                    Debug.Log("Sorry, user was not created!" + createUserTask.Exception);
                    return;
                }
                //if anything goes wrong, otherwise
                Firebase.Auth.FirebaseUser myNewUser = createUserTask.Result;
                Debug.Log("Your nice new user is:" + myNewUser.DisplayName + " " + myNewUser.UserId);
                //THIS IS WHAT HAPPENS AT THE END OF THE ASYNC TASK
            });  

        //*a better way to wait until the end of the coroutine*//
        yield return new WaitUntil(() => createusertask.IsCompleted);
    }
    
    IEnumerator signInToFirebase()
    {
        auth = FirebaseAuth.DefaultInstance;

        //the outside task is a DIFFERENT NAME to the anonymous inner class
        Task signintask = auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(
             signInTask =>
             {
                 if (signInTask.IsCanceled)
                 {
                     //write cancelled in the console
                     Debug.Log("Cancelled!");
                     return;
                 }
                 if (signInTask.IsFaulted)
                 {
                     //write the actual exception in the console
                     Debug.Log("Something went wrong!" + signInTask.Exception);
                     return;
                 }

                 Firebase.Auth.FirebaseUser loggedInUser = signInTask.Result;
                 Debug.Log("User " + loggedInUser.DisplayName + " has logged in!");
                 
               
             }
            );
      

        yield return new WaitUntil(() => signintask.IsCompleted);

        Debug.Log("User has signed in");
    }

    public IEnumerator getNumberOfRecords()
    {
       Task numberofrecordstask =  reference.GetValueAsync().ContinueWithOnMainThread(
            getValueTask =>
            {
                if (getValueTask.IsFaulted)
                {
                    Debug.Log("Error getting data " + getValueTask.Exception);
                }

                if (getValueTask.IsCompleted)
                {
                    DataSnapshot snapshot = getValueTask.Result;
                    Debug.Log(snapshot.ChildrenCount);
                    numberOfRecords = (int)snapshot.ChildrenCount;
             
                }


            }
            );
        /*
        while (!numberofrecordsretreived)
            yield return null;*/

        yield return new WaitUntil(() => numberofrecordstask.IsCompleted);


    }
    
     IEnumerator getDataFromFirebase(string childLabel)
    {

        Task getdatatask = reference.Child(childLabel).GetValueAsync().ContinueWithOnMainThread(
            getValueTask =>
            {
                if (getValueTask.IsFaulted)
                {
                    Debug.Log("Error getting data " + getValueTask.Exception);
                }

                if (getValueTask.IsCompleted)
                {
                    DataSnapshot snapshot = getValueTask.Result;
                    //Debug.Log(snapshot.Value.ToString());

                    //snapshot object is casted to an instance of its type
                    myDataDictionary = (Dictionary<string, object>)snapshot.Value;


                    //    Debug.Log("Data received");
                    
                }


            }
            );
        //shock absorber
        /* while (!displaydata)
         {
             //the data has NOT YET been saved to snapshot
             yield return null;

         }*/

        yield return new WaitUntil(() => getdatatask.IsCompleted);

        //the data has been saved to snapshot here
        yield return StartCoroutine(displayData());
    }


    public IEnumerator getOtherPlayerKey(Player otherPlayer , gameManager g)
    {
        Task getotherplayer = reference.GetValueAsync().ContinueWithOnMainThread(
            getOtherPlayerTask =>
                {
                    if (getOtherPlayerTask.IsFaulted)
                    {
                        Debug.Log("Error getting data " + getOtherPlayerTask.Exception);
                    }
                    if (getOtherPlayerTask.IsCompleted)
                    {
                        DataSnapshot snapshot = getOtherPlayerTask.Result;
                        //Debug.Log(snapshot.Value.ToString());

                        //snapshot object is casted to an instance of its type
                        myDataDictionary = (Dictionary<string, object>)snapshot.Value;


                    }
                }
            
            );

        //wait until I'm done.
        yield return new WaitUntil(() => getotherplayer.IsCompleted);
        //all the data inside mydatadictionary
        //this gets me the second key (which is what I want)

        foreach (var element in myDataDictionary)
        {
            if (!(g.currentPlayerKey == element.Key.ToString()))
            {
                
                g.enemyPlayerKey = element.Key.ToString();
            }
        }


        //register shots listener


        /*DatabaseReference enemyshotReference = reference.Child(g.enemyPlayerKey).Child("Shots");
        DatabaseReference myshotReference = reference.Child(g.currentPlayerKey).Child("Shots");

        enemyshotReference.ChildAdded += (sender, args) => handleEnemyShot(sender, args, g);
        myshotReference.ChildChanged += (sender, args) => handleMyShot(sender, args, g);*/


        //Debug.Log(myDataDictionary.Keys.ToList());

    }

    public  IEnumerator getAllDataFromFirebase()
    {

        Task getdatatask = reference.GetValueAsync().ContinueWithOnMainThread(
            getValueTask =>
            {
                if (getValueTask.IsFaulted)
                {
                    Debug.Log("Error getting data " + getValueTask.Exception);
                }

                if (getValueTask.IsCompleted)
                {
                    DataSnapshot snapshot = getValueTask.Result;
                    //Debug.Log(snapshot.Value.ToString());

                    //snapshot object is casted to an instance of its type
                    myDataDictionary = (Dictionary<string, object>)snapshot.Value;

                  
                }


            }
            );
        

        yield return new WaitUntil(() => getdatatask.IsCompleted);

        //the data has been saved to snapshot here
        yield return StartCoroutine(displayData());
    }

    IEnumerator displayData()
    {
        foreach (var element in myDataDictionary)
        {
            Debug.Log(element.Key.ToString() + "<->" + element.Value.ToString());
            yield return new WaitForSeconds(1f);
        }

        yield return null;
    }

    IEnumerator getAllData()
    {
        if (signedin){
            yield return getNumberOfRecords();
            Debug.Log(numberOfRecords);

  

            yield return getAllDataFromFirebase();

            Debug.Log("All records retreived");

            yield return null;
        }
    }


    public IEnumerator initFirebase()
    {
        if (!signedin) { 
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://chess-89471-default-rtdb.europe-west1.firebasedatabase.app/");
            reference = FirebaseDatabase.DefaultInstance.RootReference;
            addUserButton = GameObject.Find("CreateUserButton");
            addUserButton.GetComponent<Button>().onClick.AddListener(
            () =>
            {
                Debug.Log("Create User Here");
                email = GameObject.Find("EmailField").GetComponent<InputField>().text;
                password = GameObject.Find("PasswordField").GetComponent<InputField>().text;
                StartCoroutine(createUser());
                addUserButton.GetComponent<Button>().interactable = false;
                //we need to wait for createUser to be done
            });
            loginButton = GameObject.Find("LoginButton");
            loginButton.GetComponent<Button>().onClick.AddListener(OnClickLogin);
            yield return signInToFirebase();
            Debug.Log("Firebase Initialized!");
            yield return true;
            signedin = true;
            getDataButton = GameObject.Find("JoinButton");
            getDataButton.GetComponent<Button>().onClick.AddListener(delegate {
                StartCoroutine(getAllData());
            });
        } else
        {
            yield return null;
        }
    }

    public void OnClickLogin()
    {
        Debug.Log("Start signing in");
        StartCoroutine(signInToFirebase());
        loginButton.GetComponent<Button>().interactable = false;
    }

    public static bool signedin = false;
    
    void Start()
    {
        
    }
}